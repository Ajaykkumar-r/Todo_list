var categories = [];
var li,alert,i;
var ul = document.createElement("ul");
var j = 0;
var sub = 0;

var x = document.getElementById("categorylist");
x.onclick = function(event) {
    if(event.target.className === 'delete fa fa-close') {
        var li = document.getElementById(event.target.id);
        var label = li.querySelector("label").innerText;
        var b = categories.indexOf(find(label));
        categories.splice(b, 1);
        ul = document.getElementById("mainlist");
        ul.removeChild(li);
    } else if (event.target.className === "edit fa fa-pencil") {
        edit(event);
    } else if (event.target.className === "open") {
        var li = document.getElementById(event.target.id);
        var label = li.querySelector("label");
        var category = find(label.innerText);
        open(category);
    }
}

function open(category) {
    var existing = document.getElementById("singlecategory");
        var existingdiv = existing.querySelectorAll("div");
        var existingheading = existing.querySelector("h3");
        if(null != existingheading) {
            existing.removeChild(existingheading);
            existing.removeChild(document.getElementById("collections"));
            existing.removeChild(document.getElementById("add"));
        }
    var title = document.createElement("h3");
    title.className = "title";
    title.id = "title";
    title.innerText = category.name;
    var add = document.createElement("div");
    add.className = "add";
    add.id = "add";
    var input = document.createElement("input");
    input.type = "text";
    input.placeholder = "Enter the todo name to add";
    var span = document.createElement("span");
    span.className = "addnewtodo";
    var button = document.createElement("button");
    button.className = "subcategory fa fa-plus";
    button.id = "addsubcategory"
    span.appendChild(button);
    add.appendChild(input);
    add.appendChild(span);
    var subcategories = []; 
    subcategories = category.todos;
    if(null != subcategories) {
        var collections = document.createElement("div");
        collections.id = "collections";
        for(i=1; subcategories.length >= i ; i++) {
            var subcategory = subcategories[i-1];
            var card = document.createElement("div");
            card.className = "card";
            card.id = subcategory.id;
            var cardbody = document.createElement("div");
            cardbody.className = "card-body";
            cardbody.id = subcategory.id;
            var inputbox = document.createElement("input");
            inputbox.className = "editsubcategory";
            inputbox.type = "text";
            inputbox.id = subcategory.id;
            var spanbuttons = document.createElement("span");
            var closebutton = document.createElement("button");
            closebutton.className = "right fa fa-close";
            closebutton.id = subcategory.id;
            var editbutton = document.createElement("button");
            editbutton.className = "right fa fa-pencil";
            editbutton.id = "right fa fa-pencil";
            var savebutton = document.createElement("button");
            savebutton.className = "savesubcategory";
            savebutton.innerText = "Save";
            savebutton.id = subcategory.id;
            spanbuttons.appendChild(closebutton);
            spanbuttons.appendChild(editbutton);
            spanbuttons.appendChild(savebutton);
            cardbody.innerText = subcategory.name;
            cardbody.appendChild(inputbox);
            cardbody.appendChild(spanbuttons);
            card.appendChild(cardbody);
            collections.appendChild(card);
        }
        document.getElementById("singlecategory").appendChild(title);
        document.getElementById("singlecategory").appendChild(add);
        document.getElementById("singlecategory").appendChild(collections);
    } else {
        document.getElementById("singlecategory").appendChild(title);
        document.getElementById("singlecategory").appendChild(add);
    }
}

var addsubcategory = document.getElementById("singlecategory");
addsubcategory.onclick = function(event) {
    if(event.target.id === "addsubcategory") {
        var id = sub++;
        var newsubcategoryname = this.querySelector("input").value;
        var categoryname = this.querySelector("h3").innerText;
        var category = find(categoryname);
        var subcategories = category.todos;
        var subcategory = {};
        subcategory.id = id;
        subcategory.name = newsubcategoryname;
        subcategories.push(subcategory);
        var card = document.createElement("div");
        card.className = "card";
        card.id = subcategory.id;
        var cardbody = document.createElement("div");
        cardbody.className = "card-body";
        cardbody.id = id;
        var inputbox = document.createElement("input");
        inputbox.className = "editsubcategory";
        inputbox.type = "text";
        inputbox.id = id;
        var spanbuttons = document.createElement("span");
        var closebutton = document.createElement("button");
        closebutton.className = "right fa fa-close";
        closebutton.id = id;
        var editbutton = document.createElement("button");
        editbutton.className = "right fa fa-pencil";
        editbutton.id = "right fa fa-pencil";
        var savebutton = document.createElement("button");
        savebutton.className = "savesubcategory";
        savebutton.innerText = "Save";
        savebutton.id = id;
        spanbuttons.appendChild(closebutton);
        spanbuttons.appendChild(editbutton);
        spanbuttons.appendChild(savebutton);
        cardbody.innerText = newsubcategoryname;
        cardbody.appendChild(inputbox);
        cardbody.appendChild(spanbuttons);
        card.appendChild(cardbody);
        if(null != document.getElementById("collections")) {
            document.getElementById("collections").appendChild(card);
        } else {
            var collections = document.createElement("div");
            collections.id = "collections";
            collections.appendChild(card);
            document.getElementById("singlecategory").appendChild(collections);
        }
    } else if (event.target.id.includes("right fa fa-pencil")) {
        console.log(this);
        console.log(event.target);
    }
} 

function create(name) {
    var id = j++;
    li = document.createElement("li");
    var editinput = document.createElement("input");
    var editbutton = document.createElement("button");
    var deletebutton = document.createElement("button");
    var savebutton = document.createElement("button");
    var label = document.createElement("label");
    label.innerText = name;
    editinput.type = "text";
    editinput.className = "editname";
    editbutton.className = "edit fa fa-pencil";
    deletebutton.className = "delete fa fa-close";
    savebutton.className = "save";
    ul.className = "categories";
    ul.id = "mainlist";
    li.id = id;
    label.id = id;
    label.className = "open";
    editbutton.id = id;
    deletebutton.id = id;
    li.className = "categoryname";
    li.appendChild(label);
    li.appendChild(editinput);
    li.appendChild(deletebutton);
    li.appendChild(editbutton);
    li.appendChild(savebutton);
    ul.appendChild(li);
    return ul;
}

function find(name){
    for (i=0; categories.length > i; i++) {
        var category = categories[i];
        if(category["name"] === name) {
            return category;
        }
    }
}

function addCategory() {
    var name = document.getElementById("category").value;
    if('' === name) {
        document.getElementById("alert-empty").style.display = "block";
    } else if (null != find(name)) {
        document.getElementById("alert-exist").style.display = "block";
    } else {
        var category = {};
        category.id = categories.length+1;
        category.name = name;
        category.todos = [];
        categories.push(category);
        document.getElementById("categorylist").appendChild(create(name));
    }
}

function edit(event) {
    var li = document.getElementById(event.target.id);
    var input = li.querySelector("input[type=text");
    input.id = "editname" + event.target.id;
    input.style.display = "block";
    var label = li.querySelector("label");
    label.style.display = 'none';
    var category = categories[event.target.id];
    input.value = category.name;
    var button = li.getElementsByTagName("button")[2];
    button.style.display = "block";
    button.innerText = "Save";
    li.getElementsByTagName("button")[0].style.display = "none";
    li.getElementsByTagName("button")[1].style.display = "none";
    button.onclick = function(){
        var newname = document.getElementById("editname" + event.target.id).value;
        category.name = newname;
        categories.splice(event.target.id, 1 , category);
        label.innerText = newname;
        label.style.display = 'block';
        li.appendChild(label);
        button.style.display = "none";
        input.style.display = "none";
        li.getElementsByTagName("button")[0].style.display = "block";
        li.getElementsByTagName("button")[1].style.display = "block";
        if(null != document.getElementById("title").innerText) {
            document.getElementById("title").innerText = newname;
        }
    }
}

function handle(e){
    if(e.keyCode === 13){
        addCategory();
        document.getElementById('category').value = '';
    }
}